import React from 'react';
import PropTypes from 'prop-types';

function InvoiceLine(props) {
  const { lineItem, handleRemoveLine, handleChange } = props;
  return (
    <div className="grid-row">
      <div className="grid-cell grid-cell_input">
        <label>Item</label>
        <input
            type="text"
            name="item"
            value={lineItem.item}
            placeholder="Item"
            onChange={handleChange}
          />
      </div>
      <div className="grid-cell grid-cell_input">
        <label>Qty</label>
        <input
          type="text"
          name="qty"
          value={lineItem.qty}
          placeholder="Qty"
          onChange={handleChange}
        />
      </div>
      <div className="grid-cell grid-cell_input">
        <label>Price</label>
        <input
          type="text"
          name="price"
          value={lineItem.price}
          placeholder="0.00"
          onChange={handleChange}
        />
      </div>
      <div className="grid-cell grid-cell_input">
        <label>Line Total</label>
        <input
          type="text"
          name="total"
          value={`$${Number(lineItem.total).toFixed(2)}`}
          placeholder="$0.00"
          readOnly
        />
      </div>
      <div className="grid-cell grid-cell_input">
        <label>Action</label>
        <button onClick={handleRemoveLine} className="falsey">x</button>
      </div>
    </div>
  );
}

InvoiceLine.PropTypes = {
  lineItem: PropTypes.shape({}).isRequired,
  handleRemoveLine: PropTypes.func.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default InvoiceLine;
