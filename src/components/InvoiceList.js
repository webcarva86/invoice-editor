import React from 'react';
import PropTypes from 'prop-types';
import '../css/InvoiceList.css';

import InvoiceLine from './InvoiceLine';
import InvoiceAddLine from './InvoiceAddLine';

class InvoiceList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      invoices: this.props.invoices,
      lineItem: {
        item: '',
        qty: '',
        price: '',
        total: '',
      },
      subtotal: 0,
      tax: 0,
      total: 0,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeExisting = this.handleChangeExisting.bind(this);
    this.handleAddLineItem = this.handleAddLineItem.bind(this);
    this.handleUpdateTotals = this.handleUpdateTotals.bind(this);
    this.handleRemoveLineItem = this.handleRemoveLineItem.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.invoices !== this.props.invoices) {
      this.setState({
        invoices: nextProps.invoices,
      }, () => this.handleUpdateTotals())
    }
  }

  handleChangeExisting(index, event) {
    const { target } = event;

    const focusedInvoice = this.state.invoices[index];

    let total = focusedInvoice.total;
    if (target.name === 'qty') {
      total = focusedInvoice.price * target.value;
    } else if (target.name === 'price') {
      total = focusedInvoice.qty * target.value;
    }

    const updatedItem = {
      ...focusedInvoice,
      [target.name]: target.value,
      total,
    };

    const invoices = [
      ...this.state.invoices,
    ];

    invoices[index] = updatedItem;

    this.setState({
      invoices,
    }, () => this.handleUpdateTotals());
  }

  handleChange(event) {
    const { target } = event;

    let total = this.state.lineItem.total;
    if (target.name === 'qty') {
      total = this.state.lineItem.price * target.value;
    } else if (target.name === 'price') {
      total = this.state.lineItem.qty * target.value;
    }

    const lineItem = {
      ...this.state.lineItem,
      [target.name]: target.value,
      total,
    };

    this.setState({
      lineItem,
    });
  }
  
  handleAddLineItem() {
    this.props.addInvoiceLine(this.state.lineItem);
  }

  handleRemoveLineItem(item) {
    this.props.removeInvoiceLine(item);
  }

  handleUpdateTotals() {
    const subtotal = this.state.invoices.reduce((sum, invoice) => sum + invoice.total, 0);
    const tax = subtotal * 0.05;
    const total = subtotal + tax;

    this.setState({
      subtotal,
      tax,
      total,
      lineItem: {
        item: '',
        qty: '',
        price: '',
        total: '',
      }
    });

    return this.props.updateInvoiceLines(this.state.invoices);
  }

  render() {
    return (
      <div className="InvoiceList">
        <div className="grid">
          <h3>Existing Invoices</h3>
          <div className="grid-container">
            <div className="grid-row grid-row_header">
              <div className="grid-cell grid-cell_header">Item</div>
              <div className="grid-cell grid-cell_header">Qty</div>
              <div className="grid-cell grid-cell_header">Price</div>       
              <div className="grid-cell grid-cell_header">Total</div>  
              <div className="grid-cell grid-cell_header">!</div>
            </div>
            {this.state.invoices.length === 0 && (
              <div className="grid-row">
                <div className="grid-cell">No invoice lines added</div>
              </div>
            )}
            {this.state.invoices.map((invoice, index) => (
              <InvoiceLine
                key={index}
                handleRemoveLine={() => this.handleRemoveLineItem(invoice.item)}
                handleChange={(event) => this.handleChangeExisting(index, event)}
                lineItem={invoice}
              />
            ))}
          </div>
          <h3>Add Invoice Line</h3>
          <div className="grid-container">
            <InvoiceAddLine
              lineItem={this.state.lineItem}
              handleChange={event => this.handleChange(event)}
              handleAddLineItem={this.handleAddLineItem}
              onClick={this.handleAddLineItem} 
            />
          </div>
          <div className="grid-row grid-row_totals">
            <div className="grid-cell grid-cell_totals">
              <label>Subtotal</label>
              <span>${Number(this.state.subtotal).toFixed(2)}</span>
            </div>
          </div>
          <div className="grid-row grid-row_totals">
            <div className="grid-cell grid-cell_totals">
              <label>Tax (5%)</label>
              <span>${Number(this.state.tax).toFixed(2)}</span>
            </div>
          </div>
          <div className="grid-row grid-row_totals">
            <div className="grid-cell grid-cell_totals">
              <label>Total</label>
              <span>${Number(this.state.total).toFixed(2)}</span>
            </div>
          </div>
        </div>              
      </div>
    );
  }
}

InvoiceList.PropTypes = {
  invoices: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  addInvoiceLine: PropTypes.func.isRequired,
  updateInvoiceLines: PropTypes.func.isRequired,
  removeInvoiceLine: PropTypes.func.isRequired,
};

InvoiceList.defaultProps = {
  invoices: [],
}

export default InvoiceList;
