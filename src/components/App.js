import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { addInvoiceLine, updateInvoiceLines, removeInvoiceLine, clearInvoiceLines } from '../actions/invoiceActions';
import logo from '../assets/logo.svg';
import InvoiceList from './InvoiceList';
import '../css/App.css';
import { debounce } from 'debounce';

export const App = (props) => {
  const { invoices, addInvoiceLine, removeInvoiceLine, updateInvoiceLines } = props;
  const debouncedUpdate = debounce(updateInvoiceLines, 1000);
  return (
    <div className="App">
      <div className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>Welcome to Invoice Editor</h2>
      </div>

      { /* Insert your code here */}
      <InvoiceList
        invoices={invoices.invoiceLines}
        addInvoiceLine={addInvoiceLine}
        updateInvoiceLines={debouncedUpdate} 
        removeInvoiceLine={removeInvoiceLine} 
      />
    </div>
  );
}

App.PropTypes = {
  invoices: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  addInvoiceLine: PropTypes.func.isRequired,
  updateInvoiceLines: PropTypes.func.isRequired,
  removeInvoiceLine: PropTypes.func.isRequired,
};

App.defaultProps = {
  invoices: {},
}

export const mapStateToProps = state => ({
  invoices: state.invoices,
});
export const mapDispatchToProps = dispatch => bindActionCreators({
  addInvoiceLine: data => addInvoiceLine(data),
  removeInvoiceLine: item => removeInvoiceLine(item),
  updateInvoiceLines: data => updateInvoiceLines(data),
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(App);
