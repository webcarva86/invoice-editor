const defaultState = {
  invoiceLines: [],
};

export default (state = defaultState, action)  => {
  switch (action.type) {
    case 'CLEAR_INVOICE_LINE': {
      return {
        ...state,
        invoiceLines: [],
      }
    }
    case 'ADD_INVOICE_LINE': {
      return {
        ...state,
        invoiceLines: [
          ...state.invoiceLines,
          action.payload
        ],
      }
    }
    case 'UPDATE_INVOICE_LINES': {
      return {
        ...state,
        invoiceLines: action.payload
      }
    }
    case 'REMOVE_INVOICE_LINE': {
      return {
        invoiceLines: [
          ...state.invoiceLines.filter(invoice => invoice.item !== action.payload),
        ],
      }
    }
    default: {
      return state;
    }
  }
};