import React from 'react';
import ReactDOM from 'react-dom';
import Renderer from 'react-test-renderer'
import './setupTests';
import { App, mapDispatchToProps, mapStateToProps } from '../components/App';
import InvoiceList from '../components/InvoiceList';
import InvoiceAddLine from '../components/InvoiceAddLine';
import InvoiceLine from '../components/InvoiceLine';



import { shallow } from 'enzyme';

/*App Level Test*/
describe('Test the App rendering',() =>{
	it ('renders without crashing', () => {
		shallow(<App />);
	});

	it ('renders expected elements without crashing', () => {
		const wrapper = shallow(<App />);
		expect(wrapper.find('div.App').length).toEqual(1);
		expect(wrapper.find('div.App-header').length).toEqual(1);
		expect(wrapper.find('img.App-logo').length).toEqual(1);
	});

	it ('renders header without crashing', () => {
		const wrapper = shallow(<App />);
		const welcome = <h2>Welcome to Invoice Editor</h2>;
		expect(wrapper.contains(welcome)).toEqual(true);
	});

	it('mapStateToProps should return the right value', () => {
		const mockedState = {
			invoices: [
				{ item: 'terrier', qty: "3", price: "600", total: 1800 }
			]
		};
		const state = mapStateToProps(mockedState);
		expect(state).toEqual({
			invoices: [
				{ item: 'terrier', qty: "3", price: "600", total: 1800 }
			]
		});
	});

	it('mapDispatchToProps should return the right value', () => {
		const mockedDispatch = {
			addInvoiceLine: () => jest.fn(),
			removeInvoiceLine: () => jest.fn(),
			updateInvoiceLines: () => jest.fn()
		};
		const dispatch = mapDispatchToProps(mockedDispatch);
		expect(dispatch).toEqual({
			addInvoiceLine: expect.any(Function),
			removeInvoiceLine: expect.any(Function),
			updateInvoiceLines: expect.any(Function)
		});
	});

	describe('Testing InvoiceList',() => {
		let props;
		let wrapper;
		beforeEach(() => {
			props = {
				invoices: [
					{},
				],
				updateInvoiceLines: jest.fn(),
				addInvoiceLine: jest.fn(),
				removeInvoiceLine: jest.fn(),
			};
			wrapper = shallow(<InvoiceList  {...props} />);
		});

		it ('renders InvoiceList headers', () => {
			expect(wrapper.find('div.grid-cell_header').length).toEqual(5);
			expect(wrapper.instance().state.invoices).toEqual([{}]);
		});

		it ('renders expected number of InvoiceLines', () => {
			wrapper.setProps({
				invoices: [
					{item: 'dog', qty: '10', price: 999.99, total: 9999.99},
				],
			});
			const invoiceline = wrapper.find(InvoiceLine);
			expect(invoiceline.length).toEqual(1);
		});

		it('calling handleChange will update state and call totals handleUpdateTotals', () => {
			wrapper.instance().handleChange({
				target: { 
					name: 'item',
					value: 'samoyed'
				}
			});
			expect(wrapper.instance().state.lineItem).toEqual({ item: 'samoyed', qty: '', price: '', total: '' });
		});

		it('calling handleChangeExisting will update state and call totals update', () => {
			const spy = spyOn(wrapper.instance(), 'handleUpdateTotals');
			wrapper.setState({
				invoices: [
					{ item: 'dog', qty: 2, price: 200, total: 400 }
				],
			});
			wrapper.instance().handleChangeExisting(0, {
				target: { 
					name: 'item',
					value: 'samoyed'
				}
			});
			expect(wrapper.instance().state.invoices[0]).toEqual({ item: 'samoyed', qty: 2, price: 200, total: 400 });
			expect(spy).toBeCalled();
		});

		it('calling local functions should call parent/next functions', () => {
			wrapper.instance().handleAddLineItem();
			expect(props.addInvoiceLine).toBeCalledWith(wrapper.instance().state.lineItem);

			wrapper.instance().handleRemoveLineItem(0);
			expect(props.removeInvoiceLine).toBeCalledWith(0);

			wrapper.instance().handleUpdateTotals();
			expect(props.updateInvoiceLines).toBeCalledWith(wrapper.instance().state.invoices);	

			const spy = spyOn(wrapper.instance(), 'componentWillReceiveProps');

			wrapper.setProps({
				invoices: [
					{item: 'newdog', qty: '10', price: 999.99, total: 9999.99},
				],
			});
			expect(spy).toBeCalled();
		});

		it('calling handleChange update math', () => {
			wrapper.setProps({
				invoices: [
					{item: 'dog', qty: '10', price: 999.99, total: 9999.99},
				],
			});
			wrapper.instance().handleChange({
				target: { 
					name: 'item',
					value: 'samoyed'
				}
			});
			wrapper.instance().handleChange({
				target: { 
					name: 'qty',
					value: '2'
				}
			});
			wrapper.instance().handleChange({
				target: { 
					name: 'price',
					value: '500'
				}
			});
			expect(wrapper.instance().state.lineItem).toEqual({ item: 'samoyed', qty: "2", price: "500", total: 1000 });
			wrapper.instance().handleChangeExisting(0, {
				target: { 
					name: 'item',
					value: 'terrier'
				}
			});
			wrapper.instance().handleChangeExisting(0, {
				target: { 
					name: 'qty',
					value: '3'
				}
			});
			wrapper.instance().handleChangeExisting(0, {
				target: { 
					name: 'price',
					value: '600'
				}
			});
			wrapper.update();
			expect(wrapper.instance().state.invoices[0]).toEqual({ item: 'terrier', qty: "3", price: "600", total: 1800 });
		});
	});

	describe('Testing InvoiceLine',() =>{
		it ('renders changing an input value to call handleChange expected number of times', () => {
			const props = {
				lineItem: { item: '', qty: '', price: 0, total: 0},
				handleAddLineItem: jest.fn(),
				handleChange: jest.fn(),
			}
			const wrapper = shallow(<InvoiceLine {...props} />);
			wrapper.setProps({...props});
			const itemName = 	wrapper.find('input').at(0);
			itemName.simulate('change', {
				target: { value: 'samoyed' }
			});
			wrapper.find('input').at(1).simulate('change', {
				target: { value: '1' }
			});
			wrapper.find('input').at(2).simulate('change', {
				target: { value: '5.00' }
			});
			expect(props.handleChange).toBeCalledTimes(3);
			/* TODO Fix this/Check this through parent */
			wrapper.setProps({
				lineItem: { item: 'samoyed', qty: '1', price: 2, total: 2},
			})
			const itemNameUpdated = wrapper.find('input').at(0);
			expect(itemNameUpdated.props().value).toEqual('samoyed');
		});
	});

	describe('Testing InvoiceAddLine',() =>{
		it ('renders InvoiceAddLine without crashing', () => {
			const props = {
				lineItem: { item: '', qty: '', price: 0, total: 0},
				handleAddLineItem: jest.fn(),
				handleChange: jest.fn(),
			}
			shallow(<InvoiceAddLine {...props} />);
		});

		it ('InvoiceAddLine changing an input value to call handleChange the expected number of time', () => {
			const props = {
				lineItem: { item: '', qty: '', price: 0, total: 0},
				handleAddLineItem: jest.fn(),
				handleChange: jest.fn(),
			}
			const wrapper = shallow(<InvoiceAddLine {...props} />);
			wrapper.setProps({...props});
			const itemName = 	wrapper.find('input').at(0);
			itemName.simulate('change', {
				target: { value: 'samoyed' }
			});
			wrapper.find('input').at(1).simulate('change', {
				target: { value: '1' }
			});
			wrapper.find('input').at(2).simulate('change', {
				target: { value: '2.00' }
			});
			expect(props.handleChange).toBeCalledTimes(3);
		});
		
		it ('InvoiceAddLine without checking add button enabled/disabled', () => {
			const props = {
				lineItem: { item: '', qty: '', price: 0, total: 0},
				handleAddLineItem: jest.fn(),
				handleChange: jest.fn(),
			}
			const wrapper = shallow(<InvoiceAddLine {...props} />);
			const button = wrapper.find('button');
			expect(button.props().disabled).toEqual(true);
			
			wrapper.setProps({
				lineItem: { item: 'terrier', qty: '1', price: 2, total: 2},
			})
			const updatedButton = wrapper.find('button');
			expect(updatedButton.props().disabled).toEqual(false);
		});
	});
});