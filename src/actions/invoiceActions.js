export const addInvoiceLine = (invoiceLine) => dispatch => {
  dispatch({
   type: 'ADD_INVOICE_LINE',
   payload: invoiceLine,
  })
 };

 export const updateInvoiceLines = data => dispatch => {
  dispatch({
   type: 'UPDATE_INVOICE_LINES',
   payload: data,
  })
 };

 export const removeInvoiceLine = item => dispatch => {
  dispatch({
   type: 'REMOVE_INVOICE_LINE',
   payload: item,
  })
 };

 export const clearInvoiceLines = () => dispatch => {
  dispatch({
   type: 'CLEAR_INVOICE_LINES',
  })
 };